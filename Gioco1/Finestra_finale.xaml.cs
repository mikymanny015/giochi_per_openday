﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Gioco1
{
    /// <summary>
    /// Logica di interazione per Finestra_finale.xaml
    /// </summary>
    public partial class Finestra_finale : Window
    {
        Livello3 l3 = new Livello3();
        public Finestra_finale()
        {
            WindowState = WindowState.Maximized;
            InitializeComponent();
        }

        private void On_Click(object sender, RoutedEventArgs e)
        {
            Button b = new Button();
            b = (Button)sender;
            if (b == Ricomincialivello)
            {
                
                this.Hide();
                l3.ShowDialog();
            }
            if (b == Esci)
            {
                MainWindow main = new MainWindow();
                this.Hide();
                main.ShowDialog();
               // Application.Current.Shutdown();
            }
            if (b == Ricominciagioco)
            {
                InizioGioco inizioGioco = new InizioGioco();
                this.Hide();
                inizioGioco.ShowDialog();
            }
        }
    }
}
