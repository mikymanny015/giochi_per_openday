﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Gioco1
{
    /// <summary>
    /// Logica di interazione per InizioGioco.xaml
    /// </summary>
    public partial class InizioGioco : Window
    {
        public bool pippo=true;

        public InizioGioco()
        {
            string s = "Ciao, benvenuto in qubi\n  per muoverti usa i seguenti tasti :";
            WindowState = WindowState.Maximized;
            InitializeComponent();
            l1.Content = s;
        }

        private void Avanti_Click(object sender, RoutedEventArgs e)
        {
            Button b1 = new Button();
            b1 = (Button)sender;
            if (b1 == Avanti)
            {
                Livello0 l0 = new Livello0();
                this.Hide();
                l0.ShowDialog();
            }
            if (b1 == Indietro)
            {
                MainWindow mainWindow = new MainWindow();
                this.Hide();
                mainWindow.ShowDialog();
            }
        }
    }
}
