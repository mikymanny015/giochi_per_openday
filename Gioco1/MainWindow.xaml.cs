﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace Gioco1
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
       
        //public bool valore=true;
        public MainWindow()
        {
            /*
            // da rimuovere una volta finito il beta testing 
            Livello1 l1 = new Livello1();
            l1.ShowDialog();
            this.Hide();
            // da rimuovere 
            Livello2 l2 = new Livello2();
            this.Hide();
            l2.ShowDialog();
            // lui parte da qui 
            if (valore == true)
            {
                inizioGioco.Show();// non rimane aperta 
                valore = false;
                this.Hide();
            }
            Livello3 l3 = new Livello3();
            this.Hide();
            l3.ShowDialog();
            */



            /*
            Livello1 l1 = new Livello1();
            this.Hide();
            l1.ShowDialog();
            */

            // vero inizio del programma 
            WindowState = WindowState.Maximized;
            InitializeComponent();
            //esci.Visibility = Visibility.Collapsed;
            label.Content = "Benvenuto, seleziona il gioco \n che vuoi fare";
            // da mettere per l'openday 
           // esci.Visibility = Visibility.Collapsed;
        }

        private void Seleziona_Click(object sender, RoutedEventArgs e)
        {
            InizioGioco iniziogame = new InizioGioco();
            ColpisciLaTalpa colpisciLaTalpa = new ColpisciLaTalpa();
            Ping_pong ping = new Ping_pong();
            Ostacoli_girevoli ostacoli_Girevoli = new Ostacoli_girevoli();

            Button b = new Button();
            b = (Button)sender;
            if (b == talpa)
            {
                this.Hide();
                colpisciLaTalpa.ShowDialog();
            }
            if (b == qubi)
            {
                this.Hide();
                iniziogame.ShowDialog();
            }
            if (b == esci)
            {
                Application.Current.Shutdown();
            }
            if (b == ping_pong)
            {
                this.Hide();
                ping.ShowDialog();
            }
            if (b == ostacoli)
            {
                this.Hide();
                ostacoli_Girevoli.ShowDialog();
            }
        }


    }
}

