﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation; 
using System.Windows.Shapes;
using System.Windows.Threading;

namespace Gioco1
{
    /// <summary>
    /// Logica di interazione per Livello2.xaml
    /// </summary>
    public partial class Livello2 : Window
    {
        Button Pedone = new Button();
        Button Arrivo = new Button();
        // ostacoli fissi 
        Button b1 = new Button();
        Button b2 = new Button();
        Button b3 = new Button();
        Button b4 = new Button();
        Button b5 = new Button();
        Button b6 = new Button();
        Button b7 = new Button();
        Button b8 = new Button();
        Button b9 = new Button();
        Button b10 = new Button();
        Button b11 = new Button();
        Button b12 = new Button();
        Button b13 = new Button();
        Button b14 = new Button();
        Button b15 = new Button();
        Button b16 = new Button();
        Button b17 = new Button();
        Button b18 = new Button();
        Button b19 = new Button();
        Button b20 = new Button();
        Button b21 = new Button();
        Button b22 = new Button();
        Button b23 = new Button();
        Button b24 = new Button();
        Button b25 = new Button();
        Button b26 = new Button();
        Button b27 = new Button();
        Button b28 = new Button();
        Button b29 = new Button();
        Button b30 = new Button();
        Button b31= new Button();
        Button b32 = new Button();
        Button b33 = new Button();
        Button b34 = new Button();
        Button b35 = new Button();
        Button b36 = new Button();
        Button b37 = new Button();
        Button b38 = new Button();
        Button b39 = new Button();
        Button b40 = new Button();
        Button b41 = new Button();
        // bottoni mobili 
        Button b42 = new Button();
        Button b43 = new Button();
        Button b44 = new Button();
        DispatcherTimer timer1 = new DispatcherTimer();
        public double tempo;
        public Livello2()
        {
            Grid miogrid2 = new Grid();
            miogrid2.Background = Brushes.Black;
            for(int i = 0; i < 10; i++)
            {
                miogrid2.ColumnDefinitions.Add(new ColumnDefinition());
            }
            for(int i = 0; i < 10; i++)
            {
                miogrid2.RowDefinitions.Add(new RowDefinition());
            }
            miogrid2.ShowGridLines = true;// rendo visibile le colonne  
            miogrid2.Children.Add(Pedone);
            miogrid2.Children.Add(Arrivo);

            miogrid2.Children.Add(b1);
            miogrid2.Children.Add(b2);
            miogrid2.Children.Add(b3);
            miogrid2.Children.Add(b4);
            miogrid2.Children.Add(b5);
            miogrid2.Children.Add(b6);
            miogrid2.Children.Add(b7);
            miogrid2.Children.Add(b8);
            miogrid2.Children.Add(b9);
            miogrid2.Children.Add(b10);
            miogrid2.Children.Add(b11);
            miogrid2.Children.Add(b12);
            miogrid2.Children.Add(b13);
            miogrid2.Children.Add(b14);
            miogrid2.Children.Add(b15);
            miogrid2.Children.Add(b16);
            miogrid2.Children.Add(b17);
            miogrid2.Children.Add(b18);
            miogrid2.Children.Add(b19);
            miogrid2.Children.Add(b20);
            miogrid2.Children.Add(b21);
            miogrid2.Children.Add(b22);
            miogrid2.Children.Add(b23);
            miogrid2.Children.Add(b24);
            miogrid2.Children.Add(b25);
            miogrid2.Children.Add(b26);
            miogrid2.Children.Add(b27);
            miogrid2.Children.Add(b28);
            miogrid2.Children.Add(b29);
            miogrid2.Children.Add(b30);
            miogrid2.Children.Add(b31);
            miogrid2.Children.Add(b32);
            miogrid2.Children.Add(b33);
            miogrid2.Children.Add(b34);
            miogrid2.Children.Add(b35);
            miogrid2.Children.Add(b36);
            miogrid2.Children.Add(b37);
            miogrid2.Children.Add(b38);
            miogrid2.Children.Add(b39);
            miogrid2.Children.Add(b40);
            miogrid2.Children.Add(b41);
            // bottoni mobili 
            miogrid2.Children.Add(b42);
            miogrid2.Children.Add(b43);
            miogrid2.Children.Add(b44);
            timer1.Interval = TimeSpan.FromMilliseconds(0);
            timer1.Tick += Timer1_Tick;

            Inizializza();
            WindowState = WindowState.Maximized;
            InitializeComponent();
            L2.AddChild(miogrid2);
            L2.KeyDown += Direzione;
            timer1.Start();
        }
        private void Timer1_Tick(object sender, EventArgs e)
        {
            int bc = Grid.GetColumn(Pedone);
            int br = Grid.GetRow(Pedone);
            b15.Content = timer1.Interval;
            b15.FontSize = 11;
            timer1.Interval += TimeSpan.FromMilliseconds(100);
            double t = timer1.Interval.TotalMilliseconds;
            if (t % 2 == 0)// confronto se è pari 
            { // sposto il player 
                if ((bc == 4) && (br == 7))
                {
                    Grid.SetColumn(Pedone, 2);
                    Grid.SetRow(Pedone, 7);
                }
                if ((bc == 2) && (br == 3))
                {
                    Grid.SetColumn(Pedone, 2);
                    Grid.SetRow(Pedone, 1);
                }
                if ((bc == 6) && (br == 2))
                {
                    Grid.SetColumn(Pedone, 6);
                    Grid.SetRow(Pedone, 4);
                }
            }
        }
        public void Direzione(object sender,KeyEventArgs e)
        {
            int br=Grid.GetRow(Pedone);
            int bc=Grid.GetColumn(Pedone);
            if (e.Key.Equals(Key.W))// righe 
            {
                if(br<=0)
                {
                    br = 0;
                    Grid.SetRow(Pedone, br);// non lo facciamo andare troppo in alto 
                }
                else
                {
                    Grid.SetRow(Pedone, br - 1);// lo faccio salire decrementando le righe 
                }
                br = 0;
            }
            if (e.Key.Equals(Key.S))
            {
                if (br >= 9)
                {
                    br = 9;
                    Grid.SetRow(Pedone, br);// non lo faccio uscire dai boridi 
                }
                else
                    Grid.SetRow(Pedone, br + 1);// lo faccio scendere aumentando le righe 
            }
            if (e.Key.Equals(Key.A))
            {
                if (bc <= 0)
                {
                    bc = 0;
                    Grid.SetColumn(Pedone, bc);
                }
                else
                    Grid.SetColumn(Pedone, bc - 1);// lo faccio andare a sinistra agendo sulle colonne 
                bc = 0;
            }
            if (e.Key.Equals(Key.D))
            {
                if (bc >= 9)
                {
                    bc = 9;
                    Grid.SetColumn(Pedone, bc);
                }
                else
                    Grid.SetColumn(Pedone, bc + 1);// lo faccio spostare a destra 
                bc = 0;
            }
            Gestione_ostacoli();
        }
        public void Inizializza()
        {// pedone 
            Grid.SetColumn(Pedone, 9);
            Grid.SetRow(Pedone, 0);
            Pedone.Background = Brushes.Green;
            Pedone.Content = "Player";
            // arrivo 
            Grid.SetColumn(Arrivo, 4);
            Grid.SetRow(Arrivo, 5);
            Arrivo.Background = Brushes.Blue;
            Arrivo.Content = "Arrivo";
            // ostacoli 
            // b1
            Grid.SetColumn(b1, 8);
            Grid.SetRow(b1, 0);
            b1.Background = Brushes.Red;
            // b2
            Grid.SetColumn(b2, 9);
            Grid.SetRow(b2, 2);
            b2.Background = Brushes.Red;
            // b3 
            Grid.SetColumn(b3, 8);
            Grid.SetRow(b3, 4);
            b3.Background = Brushes.Red;
            // b4
            Grid.SetColumn(b4, 9);
            Grid.SetRow(b4, 6);
            b4.Background = Brushes.Red;
            // b5
            Grid.SetColumn(b5, 8);
            Grid.SetRow(b5, 8);
            b5.Background = Brushes.Red;
            // b6
            Grid.SetColumn(b6, 7);
            Grid.SetRow(b6, 8);
            b6.Background = Brushes.Red;
            // b7
            Grid.SetColumn(b7, 7);
            Grid.SetRow(b7, 7);
            b7.Background = Brushes.Red;
            // b8
            Grid.SetColumn(b8, 7);
            Grid.SetRow(b8, 6);
            b8.Background = Brushes.Red;
            //b9
            Grid.SetColumn(b9, 7);
            Grid.SetRow(b9, 5);
            b9.Background = Brushes.Red;
            // 10
            Grid.SetColumn(b10, 7);
            Grid.SetRow(b10, 4);
            b10.Background = Brushes.Red;
            // 11
            Grid.SetColumn(b11, 7);
            Grid.SetRow(b11, 3);
            b11.Background = Brushes.Red;
            // 12
            Grid.SetColumn(b12, 7);
            Grid.SetRow(b12, 2);
            b12.Background = Brushes.Red;
            // 13
            Grid.SetColumn(b13, 7);
            Grid.SetRow(b13, 1);
            b13.Background = Brushes.Red;
            // 14 
            Grid.SetColumn(b14, 6);
            Grid.SetRow(b14, 1);
            b14.Background = Brushes.Red;
            // 15 
            Grid.SetColumn(b15, 6);
            Grid.SetRow(b15, 0);
            b15.Background = Brushes.Red;
            // 16
            Grid.SetColumn(b16, 5);
            Grid.SetRow(b16, 9);
            b16.Background = Brushes.Red;
            // 17
            Grid.SetColumn(b17, 4);
            Grid.SetRow(b17, 9);
            b17.Background = Brushes.Red;
            // 18
            Grid.SetColumn(b18, 5);
            Grid.SetRow(b18, 7);
            b18.Background = Brushes.Red;
            //19
            Grid.SetColumn(b19, 6);
            Grid.SetRow(b19, 7);
            b19.Background = Brushes.Red;
            // 20
            Grid.SetColumn(b20, 3);
            Grid.SetRow(b20, 6);
            b20.Background = Brushes.Red;
            //21
            Grid.SetColumn(b21, 4);
            Grid.SetRow(b21, 6);
            b21.Background = Brushes.Red;
            // 22 
            Grid.SetColumn(b22, 2);
            Grid.SetRow(b22, 8);
            b22.Background = Brushes.Red;
            // 23
            Grid.SetColumn(b23, 3);
            Grid.SetRow(b23, 8);
            b23.Background = Brushes.Red;
            //24
            Grid.SetColumn(b24, 1);
            Grid.SetRow(b24, 7);
            b24.Background = Brushes.Red;
            //25
            Grid.SetColumn(b25, 0);
            Grid.SetRow(b25, 6);
            b25.Background = Brushes.Red;
            //26
            Grid.SetColumn(b26, 0);
            Grid.SetRow(b26, 5);
            b26.Background = Brushes.Red;
            //27
            Grid.SetColumn(b27, 0);
            Grid.SetRow(b27, 4);
            b27.Background = Brushes.Red;
            //28
            Grid.SetColumn(b28, 2);
            Grid.SetRow(b28, 5);
            b28.Background = Brushes.Red;
            //29
            Grid.SetColumn(b29, 3);
            Grid.SetRow(b29, 5);
            b29.Background = Brushes.Red;
            // 30 
            Grid.SetColumn(b30,3);
            Grid.SetRow(b30, 4);
            b30.Background = Brushes.Red;
            //31
            Grid.SetColumn(b31, 1);
            Grid.SetRow(b31, 3);
            b31.Background = Brushes.Red;
            // 32
            Grid.SetColumn(b32, 1);
            Grid.SetRow(b32, 2);
            b32.Background = Brushes.Red;
            //33
            Grid.SetColumn(b33, 1);
            Grid.SetRow(b33, 1);
            b33.Background = Brushes.Red;
            //34
            Grid.SetColumn(b34, 2);
            Grid.SetRow(b34, 0);
            b34.Background = Brushes.Red;
            // 35 
            Grid.SetColumn(b35, 3);
            Grid.SetRow(b35, 2);
            b35.Background = Brushes.Red;
            // 36
            Grid.SetColumn(b36, 4);
            Grid.SetRow(b36, 1);
            b36.Background = Brushes.Red;
            // 37 
            Grid.SetColumn(b37, 4);
            Grid.SetRow(b37, 2);
            b37.Background = Brushes.Red;
            //38
            Grid.SetColumn(b38, 5);
            Grid.SetRow(b38, 5);
            b38.Background = Brushes.Red;
            //39
            Grid.SetColumn(b39, 6);
            Grid.SetRow(b39, 5);
            b39.Background = Brushes.Red;
            // 40 
            Grid.SetColumn(b40, 3);
            Grid.SetRow(b40, 3);
            b40.Background = Brushes.Red;
            //41
            Grid.SetColumn(b41, 5);
            Grid.SetRow(b41, 3);
            b41.Background = Brushes.Red;
            // ostacoli mobili 
            // 42
            Grid.SetColumn(b42, 3);
            Grid.SetRow(b42, 7);
            b42.Background = Brushes.Yellow;
            //43
            Grid.SetColumn(b43, 2);
            Grid.SetRow(b43, 2);
            b43.Background = Brushes.Yellow;
            //44
            Grid.SetColumn(b44, 6);
            Grid.SetRow(b44, 3);
            b44.Background = Brushes.Yellow;
        }
        public void Gestione_ostacoli()
        {
            int bc = Grid.GetColumn(Pedone);
            int br = Grid.GetRow(Pedone);
            //arrivo 
            if ((bc == 4) && (br == 5))
            {
                Finelivello2finesta f2 = new Finelivello2finesta();
                f2.l1.Content = "Arrivato !! il tuo tempo è \n" + timer1.Interval+" millisecondi";
                f2.tempo = tempo + timer1.Interval.TotalMilliseconds;
                this.Hide();
                f2.ShowDialog();
            }
            //b1
            if ((bc == 8) && (br == 0))
            {
                Grid.SetColumn(Pedone, 9);
                Grid.SetRow(Pedone, 0);
            }
            //b2
            if ((bc == 9) && (br==2)) {
                Grid.SetColumn(Pedone, 9);
                Grid.SetRow(Pedone, 1);
            }
            //b3
            if ((bc == 8) && (br==4))
            {
                Grid.SetColumn(Pedone, 8);
                Grid.SetRow(Pedone, 3);
            }
            //b4
            if ((bc == 9) && (br == 6))
            {
                Grid.SetColumn(Pedone, 9);
                Grid.SetRow(Pedone, 5);
            }
            //b5
            if ((bc == 8) && (br == 8))
            {
                Grid.SetColumn(Pedone, 8);
                Grid.SetRow(Pedone, 7);
            }
            //b6
            if ((bc == 7) && (br == 8))
            {
                Grid.SetColumn(Pedone, 7);
                Grid.SetRow(Pedone, 9);
            }
            //b7
            if ((bc == 7) && (br == 7))
            {
                Grid.SetColumn(Pedone, 8);
                Grid.SetRow(Pedone, 7);
            }
            //b8
            if ((bc == 7) && (br == 6))
            {
                Grid.SetColumn(Pedone, 8);
                Grid.SetRow(Pedone, 6);
            }
            //b9
            if ((bc == 7) && (br == 5))
            {
                Grid.SetColumn(Pedone, 8);
                Grid.SetRow(Pedone, 5);
            }
            // b10
            if ((bc == 7) && (br == 4))
            {
                Grid.SetColumn(Pedone, 8);
                Grid.SetRow(Pedone, 4);
            }
            // b11
            if ((bc == 7) && (br == 3))
            {
                Grid.SetColumn(Pedone, 7);
                Grid.SetRow(Pedone,3);
            }
            //b12
            if ((bc == 7) && (br == 2))
            {
                Grid.SetColumn(Pedone, 8);
                Grid.SetRow(Pedone, 2);
            }
            //b13
            if ((bc == 7) && (br == 1))
            {
                Grid.SetColumn(Pedone, 8);
                Grid.SetRow(Pedone, 1);
            }
            //b14
            if ((bc == 6) && (br == 1))
            {
                Grid.SetColumn(Pedone, 5);
                Grid.SetRow(Pedone, 1);
            }
            // b15
            if ((bc == 6) && (br == 0))
            {
                Grid.SetColumn(Pedone, 5);
                Grid.SetRow(Pedone, 0);
            }
            //b16
            if ((bc == 5) && (br == 9))
            {
                Grid.SetColumn(Pedone, 6);
                Grid.SetRow(Pedone, 9);
            }
            //b17
            if ((bc == 4) && (br == 9))
            {
                Grid.SetColumn(Pedone, 4);
                Grid.SetRow(Pedone, 8);
            }
            //b18
            if ((bc == 5) && (br == 7))
            {
                Grid.SetColumn(Pedone, 5);
                Grid.SetRow(Pedone, 8);
            }
            //b19
            if ((bc == 6) && (br == 7))
            {
                Grid.SetColumn(Pedone, 6);
                Grid.SetRow(Pedone, 8);
            }
            //20
            if ((bc == 3) && (br == 6))
            {
                Grid.SetColumn(Pedone, 2);
                Grid.SetRow(Pedone, 6);
            }
            //21
            if ((bc == 4) && (br == 6))
            {
                Grid.SetColumn(Pedone, 4);
                Grid.SetRow(Pedone, 7);
            }
            //22
            if ((bc == 2) && (br == 8))
            {
                Grid.SetColumn(Pedone, 2);
                Grid.SetRow(Pedone, 7);
            }
            //23
            if ((bc == 3) && (br == 8))
            {
                Grid.SetColumn(Pedone, 4);
                Grid.SetRow(Pedone, 8);
            }
            //b24
            if ((bc == 1) && (br == 7))
            {
                Grid.SetColumn(Pedone, 2);
                Grid.SetRow(Pedone, 7);
            }
            //b25
            if ((bc == 0) && (br == 6))
            {
                Grid.SetColumn(Pedone, 1);
                Grid.SetRow(Pedone, 6);
            }
            //b26
            if ((bc == 0) && (br==5)) {
                Grid.SetColumn(Pedone, 1);
                Grid.SetRow(Pedone, 5);
            }
            //b27
            if ((bc == 0) && (br == 4))
            {
                Grid.SetColumn(Pedone, 1);
                Grid.SetRow(Pedone, 4);
            }
            //b28
            if ((bc == 2) && (br == 5))
            {
                Grid.SetColumn(Pedone, 1);
                Grid.SetRow(Pedone, 5);
            }
            //b29
            if ((bc == 3) && (br == 5))
            {
                Grid.SetColumn(Pedone, 4);
                Grid.SetRow(Pedone, 5);
            }
            //b30
            if ((bc == 3) && (br == 4))
            {
                Grid.SetColumn(Pedone, 2);
                Grid.SetRow(Pedone, 4);
            }
            //b31
            if ((bc == 1) && (br == 3))
            {
                Grid.SetColumn(Pedone, 1);
                Grid.SetRow(Pedone, 4);
            }
            //b32
            if ((bc == 1) && (br == 2))
            {
                Grid.SetColumn(Pedone, 2);
                Grid.SetRow(Pedone, 2);
            }
            //b33
            if ((bc == 1) && (br == 1))
            {
                Grid.SetColumn(Pedone, 2);
                Grid.SetRow(Pedone, 1);
            }
            //b34
            if ((bc == 2) && (br == 0))
            {
                Grid.SetColumn(Pedone, 2);
                Grid.SetRow(Pedone, 1);
            }
            //b35
            if ((bc == 3) && (br == 2))
            {
                Grid.SetColumn(Pedone, 3);
                Grid.SetRow(Pedone, 1);
            }
            //b36
            if ((bc == 4) && (br == 1))
            {
                Grid.SetColumn(Pedone, 3);
                Grid.SetRow(Pedone, 1);
            }
            //b37
            if ((bc == 4) && (br == 2))
            {
                Grid.SetColumn(Pedone, 5);
                Grid.SetRow(Pedone, 2);
            }
            //b38
            if ((bc == 5) && (br == 5))
            {
                Grid.SetColumn(Pedone, 5);
                Grid.SetRow(Pedone, 4);
            }
            //b39
            if ((bc == 6) && (br == 5))
            {
                Grid.SetColumn(Pedone, 6);
                Grid.SetRow(Pedone, 4);
            }
            // b40
            if ((bc == 3) && (br == 3))
            {
                Grid.SetColumn(Pedone, 2);
                Grid.SetRow(Pedone, 3);
            }
            //b41
            if ((bc == 5) && (br == 3))
            {
                Grid.SetColumn(Pedone, 5);
                Grid.SetRow(Pedone, 2);
            }

            // ostacoli mobili 
            if ((bc == 3) && (br == 7))
            {
                Grid.SetColumn(Pedone, 4);
                Grid.SetRow(Pedone, 7);
            }
            if ((bc == 2) && (br == 2))
            {
                Grid.SetColumn(Pedone, 2);
                Grid.SetRow(Pedone, 3);
            }
            if ((bc == 6) && (br == 3))
            {
                Grid.SetColumn(Pedone, 6);
                Grid.SetRow(Pedone, 2);
            }
        } 
    }
}
