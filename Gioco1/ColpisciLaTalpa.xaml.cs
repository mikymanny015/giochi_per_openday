﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace Gioco1
{
    /// <summary>
    /// Logica di interazione per ColpisciLaTalpa.xaml
    /// </summary>
    public partial class ColpisciLaTalpa : Window
    {
        Button pedina = new Button();
        Button uscita = new Button();
        Button score = new Button();
        DispatcherTimer timer = new DispatcherTimer();
        public double i = 1000;
        public int cont = 0;
        public int Colonne = 0;
        public int Righe = 0;
        Grid grid = new Grid();
        bool verifica = false;
        DispatcherTimer immaginetimer = new DispatcherTimer();
        bool cliccato = false;

        Image imgclick = new Image();
        Image img_after_clik = new Image();


        public ColpisciLaTalpa()
        {

            string s = "  Benevenuto in acchiappa spongebob \n insersici la dimensione del campo da gioco.";

            // cambio il colore 
            grid.Background = Brushes.Black;
            grid.Visibility = Visibility.Collapsed;
            // imposto il timer
            timer.Interval = TimeSpan.FromMilliseconds(i);
            timer.Tick += Timer_Tick;
            // imposto il timer per l'immagine 
            immaginetimer.Interval = TimeSpan.FromMilliseconds(0);
            immaginetimer.Tick += Immaginetimer_Tick;

            pedina.Click += Pedina_Click;
            uscita.Click += Uscita_Click;
            // aggiungo immagine 1
            imgclick.Source = new BitmapImage(new Uri("../../Immagini/immagine_click.png", UriKind.Relative));
            imgclick.Stretch = Stretch.UniformToFill;
            // agiungo immagine 2 
            img_after_clik.Source = new BitmapImage(new Uri("../../Immagini/Immagine_sfondo.png", UriKind.Relative));
            img_after_clik.Stretch = Stretch.UniformToFill;
            WindowState = WindowState.Maximized;

            InitializeComponent();
            l3.Content = s;
            mg.Children.Add(grid);
            timer.Start();
            immaginetimer.Start();

        }

        private void Immaginetimer_Tick(object sender, EventArgs e)
        {
            if (cliccato == true)
            {
                // pedina.Background = Brushes.Green;
                pedina.Content = imgclick;
               immaginetimer.Interval += TimeSpan.FromMilliseconds(1);
                pedina.IsEnabled = false;
            }

            if( (immaginetimer.Interval.TotalMilliseconds == 10)&&(cliccato==true))
            {
                cliccato = false;
                pedina.IsEnabled = true;
                //  pedina.Background = Brushes.Gray;
                pedina.Content = img_after_clik;
                immaginetimer.Interval = TimeSpan.FromMilliseconds(0);
            }

        }
        private void Uscita_Click(object sender, RoutedEventArgs e)
        {
            MainWindow main = new MainWindow();
            this.Hide();
            main.ShowDialog();
        }
        private void Pedina_Click(object sender, RoutedEventArgs e)
        {
            // ottengo un vaore 
            
            if (i <= 0)
            {
                i = 1000;
            }
            if ((i <= 200)&&(i>50))
                i = i - 50;
            if (i <= 50)
            {
                i = i - 20;
            }
            if(i > 200){
                i = i - 200;
            }
            
           // pedina.Content = i;
            cont++;
            score.Content = " " + cont + " ";
            cliccato = true;
        }
        private void Timer_Tick(object sender, EventArgs e)
        {

            // timer.Interval += TimeSpan.FromMilliseconds(i);
            if (verifica == true)
            {
                if (cont == 50)
                {
                    MessageBox.Show("hai raggiunto il numero massimo di clic consentiti");
                    MainWindow main = new MainWindow();
                    this.Hide();
                    main.ShowDialog();
                }
                bool evita_salto = false;
                int r ;
                int c ;
                Random random = new Random();
                // 8 perchè in fondo c'è l'uscita 
                c = random.Next(0, Colonne);
                r = random.Next(0, Righe);
                if ((c == (Colonne / 2)) && (r == 0))
                {
                    Grid.SetColumn(pedina, Colonne / 2);
                    Grid.SetRow(pedina, 1);
                    evita_salto = true;
                }
                if ((c == Colonne) && (r == Righe))
                {
                    Grid.SetColumn(pedina, c-1);
                    Grid.SetRow(pedina, r-1);
                }   
                else
                {
                    if (evita_salto == false)
                    {
                        Grid.SetColumn(pedina, c);
                        Grid.SetRow(pedina, r);
                    }
                }
            }
        } 
        private void Invio_Click(object sender, RoutedEventArgs e)
        {
            // t1 colonne , t2 righe 
            // rislto problema inserimento vuoto 
            if ((t1.Text == "") && (t2.Text == ""))
            {
                Colonne = 2;
                Righe = 2;
            }
            else
            {
                if (Colonne >= 15)
                {
                    Colonne = 14;
                }
                if (Righe >= 15)
                {
                    Righe = 14;
                }
                Colonne = Convert.ToInt32(t1.Text);
                Righe = Convert.ToInt32(t2.Text);
            }
            for (int i = 0; i < Colonne; i++)
            {
                grid.ColumnDefinitions.Add(new ColumnDefinition());
            }
            for (int i = 0; i < Righe; i++)
            {
                grid.RowDefinitions.Add(new RowDefinition());
            }
            grid.ShowGridLines = true;

            grid.Children.Add(pedina);
            grid.Children.Add(score);
            // setto score 
            Grid.SetRow(score, 0);
            Grid.SetColumn(score, Colonne/2);
            score.Content = "0";
            // setto la porta di uscita 
            Grid.SetRow(uscita, Righe);
            Grid.SetColumn(uscita, Colonne);
            grid.Children.Add(uscita);
            uscita.Background = Brushes.Green;
            uscita.Content = "Uscita";
            Invisible();
            grid.Visibility = Visibility.Visible;
            verifica = true;
            //pedina.Background = Brushes.Gray;
            pedina.Content = img_after_clik;

        }
        private void Invisible()
        {
            //l1,l2,l3,t1,t2,invio
            l1.Visibility = Visibility.Collapsed;
            l2.Visibility = Visibility.Collapsed;
            l3.Visibility = Visibility.Collapsed;
            t1.Visibility = Visibility.Collapsed;
            t2.Visibility = Visibility.Collapsed;
            Invio.Visibility = Visibility.Collapsed;
        }
    }
}
