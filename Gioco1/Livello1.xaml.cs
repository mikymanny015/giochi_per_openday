﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Navigation;
using System.Windows.Threading;

namespace Gioco1
{
    /// <summary>
    /// Logica di interazione per Livello1.xaml
    /// </summary>
    public partial class Livello1 : Window
    {
        Button player = new Button();
        // parte alta 
        Button b1 = new Button();
        Button b2 = new Button();
        Button b3 = new Button();
        Button b4 = new Button();
        Button b5 = new Button();
        //Button b6 = new Button();
        Button b7 = new Button();
        // parte bassa
        // Button b8 = new Button();
        Button b9 = new Button();
        Button b10 = new Button();
        Button b11 = new Button();
        Button b12 = new Button();
        Button b13 = new Button();
        Button b14 = new Button();
        Button b15 = new Button();
        Button b16 = new Button();
        // parte laterale sinistra 
        Button b17 = new Button();
        Button b18 = new Button();
        Button b19 = new Button();
        Button b20 = new Button();
        Button b21 = new Button();
        Button b22 = new Button();
        Button b23 = new Button();
        Button b24 = new Button();
        Button b25 = new Button();
        Button b26 = new Button();
        Button b27 = new Button();
        Button b28 = new Button();
        Button b29 = new Button();
        // centro 
        Button b30 = new Button();
        Button b31 = new Button();
        Button b32 = new Button();
        Button b33 = new Button();
        Button b34 = new Button();
        Button b35 = new Button();
        Button b36 = new Button();
        Button b37 = new Button();
        Button b38 = new Button();
        Button b39 = new Button();
        // destra 
        Button b40 = new Button();
        Button b41 = new Button();
        Button b42 = new Button();
        Button b43 = new Button();
        // arrivo 
        Button arrivo = new Button();
        public DispatcherTimer timer1 = new DispatcherTimer();
        public double tempo;
        public Livello1()
        {
            // creo il timer 
            timer1.Interval = TimeSpan.FromMilliseconds(0);
            timer1.Tick += Timer1_Tick;

            Grid livello1 = new Grid();
            livello1.Background = Brushes.Black;
            for (int i = 0; i < 10; i++)
            {
                livello1.ColumnDefinitions.Add(new ColumnDefinition());
            }
            for (int i = 0; i < 10; i++)
            {
                livello1.RowDefinitions.Add(new RowDefinition());
            }
            livello1.ShowGridLines = true;
            // inizializzo le variabili 
            livello1.Children.Add(player);
            livello1.Children.Add(arrivo);
            // inizializzo i blocchi 
            livello1.Children.Add(b1);
            livello1.Children.Add(b2);
            livello1.Children.Add(b3);
            livello1.Children.Add(b4);
            livello1.Children.Add(b5);
            //   livello1.Children.Add(b6);
            livello1.Children.Add(b7);
            // parte bassa 
            //  livello1.Children.Add(b8);
            livello1.Children.Add(b9);
            livello1.Children.Add(b10);
            livello1.Children.Add(b11);
            //   livello1.Children.Add(b12);
            livello1.Children.Add(b13);
            livello1.Children.Add(b14);
            livello1.Children.Add(b15);
            livello1.Children.Add(b16);
            // parte sinistra 
            livello1.Children.Add(b17);
            livello1.Children.Add(b18);
            livello1.Children.Add(b19);
            //   livello1.Children.Add(b20);
            livello1.Children.Add(b21);
            livello1.Children.Add(b22);
            livello1.Children.Add(b23);
            livello1.Children.Add(b24);
            livello1.Children.Add(b25);
            livello1.Children.Add(b26);
            livello1.Children.Add(b27);
            livello1.Children.Add(b28);
            livello1.Children.Add(b29);
            // centro 
            livello1.Children.Add(b30);
            livello1.Children.Add(b31);
            livello1.Children.Add(b32);
            livello1.Children.Add(b33);
            livello1.Children.Add(b34);
            livello1.Children.Add(b35);
            livello1.Children.Add(b36);
            livello1.Children.Add(b37);
            livello1.Children.Add(b38);
            livello1.Children.Add(b39);
            // destra 
            livello1.Children.Add(b40);
            livello1.Children.Add(b41);
            livello1.Children.Add(b42);
            livello1.Children.Add(b43);
            Instanzia();
            WindowState = WindowState.Maximized;
            InitializeComponent();
            l1.AddChild(livello1);
            l1.KeyDown += Direzione;
            timer1.Start();
        }
        private void Timer1_Tick(object sender, EventArgs e)
        {
            timer1.Interval += TimeSpan.FromMilliseconds(100);
            b3.Content = timer1.Interval;
        }
        public void Direzione(object sender, KeyEventArgs e)
        {
            int br;
            int bc;
            if (e.Key.Equals(Key.W))
            {
                br = Grid.GetRow(player);
                if (br <= 0)
                {
                    br = 0;
                    Grid.SetRow(player, br);
                }
                else
                {
                    Grid.SetRow(player, br - 1);// cosi salgo 
                }
                br = 0;
            }
            if (e.Key.Equals(Key.S))
            {
                br = Grid.GetRow(player);
                if (br >= 9)
                {
                    br = 9;
                    Grid.SetRow(player, br);
                }
                else
                    Grid.SetRow(player, br + 1);// cosi scendo 
                br = 0;
            }
            if (e.Key.Equals(Key.A))
            {
                bc = Grid.GetColumn(player);
                if (bc <= 0)
                {
                    bc = 0;
                    Grid.SetColumn(player, bc);
                }
                else
                    Grid.SetColumn(player, bc - 1);// cosi si muove a sinistra 
                bc = 0;
            }
            if (e.Key.Equals(Key.D))
            {
                bc = Grid.GetColumn(player);
                if (bc >= 9)
                {
                    bc = 9;
                    Grid.SetColumn(player, bc);
                }
                else
                    Grid.SetColumn(player, bc + 1);// si muove a destra 
                bc = 0;
            }
            GestioneOstacoli(player);
        }
        public void Instanzia()
        {
            Grid.SetRow(player, 0);
            Grid.SetColumn(player, 8);
            player.Background = Brushes.Green;
            player.Content = "player";
            player.FontSize = 20;
            // spazio per instaziare arrivo 
            Grid.SetRow(arrivo, 0);
            Grid.SetColumn(arrivo, 0);
            arrivo.Background = Brushes.Blue;
            arrivo.Content = "Arrivo";
            arrivo.FontSize = 20;
            // instazio ostacoli 
            Grid.SetRow(b1, 0);
            Grid.SetColumn(b1, 7);
            b1.Background = Brushes.Red;
            // b2
            Grid.SetRow(b2, 1);
            Grid.SetColumn(b2, 7);
            b2.Background = Brushes.Red;
            // b3
            Grid.SetRow(b3, 2);
            Grid.SetColumn(b3, 7);
            b3.Background = Brushes.Red;
            // b4
            Grid.SetRow(b4, 3);
            Grid.SetColumn(b4, 7);
            b4.Background = Brushes.Red;
            // b5
            Grid.SetRow(b5, 4);
            Grid.SetColumn(b5, 7);
            b5.Background = Brushes.Red;
            // b6
            // Grid.SetRow(b6, 5);
            //Grid.SetColumn(b6,7);
            //sb6.Background = Brushes.Red;
            // vecchi 1
            // b7
            Grid.SetRow(b7, 6);
            Grid.SetColumn(b7, 7);
            b7.Background = Brushes.Red;
            // b8
            //  Grid.SetRow(b8, 8);
            // Grid.SetColumn(b8, 9);
            //b8.Background = Brushes.Red;
            //b9
            Grid.SetRow(b9, 8);
            Grid.SetColumn(b9, 8);
            b9.Background = Brushes.Red;
            // b10
            Grid.SetRow(b10, 8);
            Grid.SetColumn(b10, 7);
            b10.Background = Brushes.Red;
            // b11
            Grid.SetRow(b11, 8);
            Grid.SetColumn(b11, 6);
            b11.Background = Brushes.Red;
            // b12
            //Grid.SetRow(b12, 8);
            //Grid.SetColumn(b12, 5);
            //sb12.Background = Brushes.Red;
            // b13
            Grid.SetRow(b13, 8);
            Grid.SetColumn(b13, 4);
            b13.Background = Brushes.Red;
            // b14
            Grid.SetRow(b14, 8);
            Grid.SetColumn(b14, 3);
            b14.Background = Brushes.Red;
            // b15
            Grid.SetRow(b15, 8);
            Grid.SetColumn(b15, 2);
            b15.Background = Brushes.Red;
            // b16
            Grid.SetRow(b16, 7);
            Grid.SetColumn(b16, 5);
            b16.Background = Brushes.Red;
            // 17
            Grid.SetRow(b17, 7);
            Grid.SetColumn(b17, 1);
            b17.Background = Brushes.Red;
            // 18
            Grid.SetRow(b18, 6);
            Grid.SetColumn(b18, 1);
            b18.Background = Brushes.Red;
            // 19 
            Grid.SetRow(b19, 5);
            Grid.SetColumn(b19, 1);
            b19.Background = Brushes.Red;
            // 20 
            // Grid.SetRow(b20, 5);
            //Grid.SetColumn(b20,0);
            //b20.Background = Brushes.Red;
            // 21
            Grid.SetRow(b21, 4);
            Grid.SetColumn(b21, 0);
            b21.Background = Brushes.Red;
            // 22 
            Grid.SetRow(b22, 3);
            Grid.SetColumn(b22, 0);
            b22.Background = Brushes.Red;
            // 23
            Grid.SetRow(b23, 2);
            Grid.SetColumn(b23, 0);
            b23.Background = Brushes.Red;
            // 24
            Grid.SetRow(b24, 1);
            Grid.SetColumn(b24, 0);
            b24.Background = Brushes.Red;
            // 25
            Grid.SetRow(b25, 1);
            Grid.SetColumn(b25, 1);
            b25.Background = Brushes.Red;
            // 26
            Grid.SetRow(b26, 1);
            Grid.SetColumn(b26, 2);
            b26.Background = Brushes.Red;
            // 27
            Grid.SetRow(b27, 1);
            Grid.SetColumn(b27, 3);
            b27.Background = Brushes.Red;
            // 28 
            Grid.SetRow(b28, 1);
            Grid.SetColumn(b28, 4);
            b28.Background = Brushes.Red;
            // 29
            Grid.SetRow(b29, 1);
            Grid.SetColumn(b29, 5);
            b29.Background = Brushes.Red;
            // 30 
            Grid.SetRow(b30, 3);
            Grid.SetColumn(b30, 6);
            b30.Background = Brushes.Red;
            // 31
            Grid.SetRow(b31, 3);
            Grid.SetColumn(b31, 5);
            b31.Background = Brushes.Red;
            //32
            Grid.SetRow(b32, 3);
            Grid.SetColumn(b32, 4);
            b32.Background = Brushes.Red;
            //33
            Grid.SetRow(b33, 3);
            Grid.SetColumn(b33, 3);
            b33.Background = Brushes.Red;
            //34
            Grid.SetRow(b34, 3);
            Grid.SetColumn(b34, 2);
            b34.Background = Brushes.Red;
            //35
            Grid.SetRow(b35, 5);
            Grid.SetColumn(b35, 6);
            b35.Background = Brushes.Red;
            //36
            Grid.SetRow(b36, 5);
            Grid.SetColumn(b36, 5);
            b36.Background = Brushes.Red;
            //37
            Grid.SetRow(b37, 5);
            Grid.SetColumn(b37, 4);
            b37.Background = Brushes.Red;
            //38
            Grid.SetRow(b38, 5);
            Grid.SetColumn(b38, 3);
            b38.Background = Brushes.Red;
            //39
            Grid.SetRow(b39, 6);
            Grid.SetColumn(b39, 3);
            b39.Background = Brushes.Red;
            // 40 
            Grid.SetRow(b40, 7);
            Grid.SetColumn(b40, 9);
            b40.Background = Brushes.Red;
            //41
            Grid.SetRow(b41, 5);
            Grid.SetColumn(b41, 8);
            b41.Background = Brushes.Red;
            //42
            Grid.SetRow(b42, 3);
            Grid.SetColumn(b42, 9);
            b42.Background = Brushes.Red;
            // 43
            Grid.SetRow(b43, 1);
            Grid.SetColumn(b43, 8);
            b43.Background = Brushes.Red;


        }
        public void GestioneOstacoli(Button player)
        {
            int c = Grid.GetColumn(player);
            int r = Grid.GetRow(player);
            if ((c == 0) && (r == 0))
            {
               FineLivello1Finestra f = new FineLivello1Finestra();
                f.l1.Content= "finito il gioco, il tempo \n che hai  impiegato è \n" + timer1.Interval+" millisecondi";
                f.tempo = tempo + timer1.Interval.TotalMilliseconds;
                this.Hide();
                f.ShowDialog();
            }
            // attivo gli ostacoli 
            //b1
            if ((c == 7) && (r == 0))
            {
                Grid.SetColumn(player, 8);
                Grid.SetRow(player, 0);
            }
            //b2
            if ((c == 7) && (r == 1))
            {
                Grid.SetColumn(player, 8);
                Grid.SetRow(player, 1);
            }
            //b3
            if ((c == 7) && (r == 2))
            {
                Grid.SetColumn(player, 8);
                Grid.SetRow(player, 2);
            }
            //b4
            if ((c == 7) && (r == 3))
            {
                Grid.SetColumn(player, 8);
                Grid.SetRow(player, 3);
            }
            //b5
            if ((c == 7) && (r == 4))
            {
                Grid.SetColumn(player, 8);
                Grid.SetRow(player, 4);
            }
            //b7
            if ((c == 7) && (r == 6))
            {
                Grid.SetColumn(player, 8);
                Grid.SetRow(player, 6);
            }
            //b9
            if ((c == 8) && (r == 8))
            {
                Grid.SetColumn(player, 8);
                Grid.SetRow(player, 7);
            }
            // 10 
            if ((c == 7) && (r == 8))
            {
                Grid.SetColumn(player, 7);
                Grid.SetRow(player, 7);
            }
            // 11
            if ((c == 6) && (r == 8))
            {
                Grid.SetColumn(player, 6);
                Grid.SetRow(player, 7);
            }
            // 16
            if ((c == 5) && (r == 7))
            {
                Grid.SetColumn(player, 6);
                Grid.SetRow(player, 7);
            }
            // 12
            if ((c == 4) && (r == 8))
            {
                Grid.SetColumn(player, 4);
                Grid.SetRow(player, 7);
            }
            // 13
            if ((c == 3) && (r == 8))
            {
                Grid.SetColumn(player, 3);
                Grid.SetRow(player, 7);
            }
            //14
            if ((c == 2) && (r == 8))
            {
                Grid.SetColumn(player, 2);
                Grid.SetRow(player, 7);
            }
            // 17
            if ((c == 1) && (r == 7))
            {
                Grid.SetColumn(player, 2);
                Grid.SetRow(player, 7);
            }
            // 18
            if ((c == 1) && (r == 6))
            {
                Grid.SetColumn(player, 2);
                Grid.SetRow(player, 6);
            }
            // 19
            if ((c == 1) && (r == 5))
            {
                Grid.SetColumn(player, 2);
                Grid.SetRow(player, 5);
            }
            // 21
            if ((c == 0) && (r == 4))
            {
                Grid.SetColumn(player, 1);
                Grid.SetRow(player, 4);
            }
            //22
            if ((c == 0) && (r == 3))
            {
                Grid.SetColumn(player, 1);
                Grid.SetRow(player, 3);
            }
            //23
            if ((c == 0) && (r == 2))
            {
                Grid.SetColumn(player, 1);
                Grid.SetRow(player, 2);
            }
            //24
            if ((c == 0) && (r == 1))
            {
                Grid.SetColumn(player, 1);
                Grid.SetRow(player, 1);
            }
            // 25
            if ((c == 1) && (r == 1))
            {
                Grid.SetColumn(player, 1);
                Grid.SetRow(player, 2);
            }
            //26
            if ((c == 2) && (r == 1))
            {
                Grid.SetColumn(player, 2);
                Grid.SetRow(player, 2);
            }
            //27
            if ((c == 3) && (r == 1))
            {
                Grid.SetColumn(player, 3);
                Grid.SetRow(player, 2);
            }
            //28
            if ((c == 4) && (r == 1))
            {
                Grid.SetColumn(player, 4);
                Grid.SetRow(player, 2);
            }
            //29
            if ((c == 5) && (r == 1))
            {
                Grid.SetColumn(player, 5);
                Grid.SetRow(player, 2);
            }
            //30
            if ((c == 6) && (r == 3))
            {
                Grid.SetColumn(player, 6);
                Grid.SetRow(player, 4);
            }
            //31
            if ((c == 5) && (r == 3))
            {
                Grid.SetColumn(player, 5);
                Grid.SetRow(player, 4);
            }
            //32
            if ((c == 4) && (r == 3))
            {
                Grid.SetColumn(player, 4);
                Grid.SetRow(player, 4);
            }
            //33
            if ((c == 3) && (r == 3))
            {
                Grid.SetColumn(player, 3);
                Grid.SetRow(player, 4);
            }
            //34
            if ((c == 2) && (r == 3))
            {
                Grid.SetColumn(player, 2);
                Grid.SetRow(player, 4);
            }
            //35
            if ((c == 6) && (r == 5))
            {
                Grid.SetColumn(player, 6);
                Grid.SetRow(player, 6);
            }
            //36
            if ((c == 5) && (r == 5))
            {
                Grid.SetColumn(player, 5);
                Grid.SetRow(player, 6);
            }
            //37
            if ((c == 4) && (r == 5))
            {
                Grid.SetColumn(player, 4);
                Grid.SetRow(player, 6);
            }
            //38
            if ((c == 3) && (r == 5))
            {
                Grid.SetColumn(player, 2);
                Grid.SetRow(player, 6);
            }
            //39
            if ((c == 3) && (r == 6))
            {
                Grid.SetColumn(player, 3);
                Grid.SetRow(player, 7);
            }
            //40
            if ((c == 9) && (r == 7))
            {
                Grid.SetColumn(player, 9);
                Grid.SetRow(player, 6);
            }
            //41
            if ((c == 8) && (r == 5))
            {
                Grid.SetColumn(player, 8);
                Grid.SetRow(player, 4);
            }
            //42
            if ((c == 9) && (r == 3))
            {
                Grid.SetColumn(player, 9);
                Grid.SetRow(player, 2);
            }
            //43
            if ((c == 8) && (r == 1))
            {
                Grid.SetColumn(player, 8);
                Grid.SetRow(player, 0);
            }
        }
    }
}
