﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;


namespace Gioco1
{
    /// <summary>
    /// Logica di interazione per Finestra.xaml
    /// </summary>
    public partial class Finestra : Window
    {
       public Livello0 m = new Livello0();
        public Livello1 l1 = new Livello1();
        public double tempo;
        public Finestra()
        {
            WindowState = WindowState.Maximized;
            InitializeComponent();
            Fine.Background = Brushes.Transparent;
            Fine.Content = "Arrivato!!!!!!!!!";
            Fine.Foreground = Brushes.Green;
            Chiudi.Visibility = Visibility.Collapsed;// rendo invisibile l'oggetto 
        }
        private void Stop(object sender,RoutedEventArgs e)
        {
            Button b = new Button();
            b = (Button)sender;
            if (b == Chiudi)
            {

                // chiudere i progetti 
                this.Hide();
                m.Close();
                this.Close();
                // cosi si chiude il programma
                System.Windows.Application.Current.Shutdown();
            }
            if (b == Ricomincia)
            {
               this.Hide();
               m.ShowDialog();
                tempo = 0;
            }
            if (b == Avanti)
            {
                l1.tempo = tempo;
                this.Hide();
                l1.ShowDialog();
                b = null;
            }

        }
    }
}
