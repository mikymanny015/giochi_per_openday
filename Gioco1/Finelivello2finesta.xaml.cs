﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Gioco1
{
    /// <summary>
    /// Logica di interazione per Finelivello2finesta.xaml
    /// </summary>
    public partial class Finelivello2finesta : Window
    {
        Livello2 l2 = new Livello2();
        public double tempo;
        public Finelivello2finesta()
        {
            WindowState = WindowState.Maximized;
            InitializeComponent();
        }

        private void On_Click(object sender, RoutedEventArgs e)
        {
            Button b1 = new Button();
            b1 = (Button)sender;
            if (b1 == Avanti)
            {
                Livello3 l3 = new Livello3();
                l3.tempo = tempo;
                this.Hide();
                l3.ShowDialog();
            }
            if (b1 == Ricomincia)
            {
                tempo = 0;
                this.Hide();// prima si chiude una finestra 
                l2.ShowDialog();// poi apriamo l'altra 
       

            }
        }
    }
}
