﻿#pragma checksum "..\..\InizioGioco.xaml" "{8829d00f-11b8-4213-878b-770e8597ac16}" "BF7788A321B79DAB9DF5E1567BFAD4205C5F5AA706B0CF1183B3BB368BD75CF0"
//------------------------------------------------------------------------------
// <auto-generated>
//     Il codice è stato generato da uno strumento.
//     Versione runtime:4.0.30319.42000
//
//     Le modifiche apportate a questo file possono provocare un comportamento non corretto e andranno perse se
//     il codice viene rigenerato.
// </auto-generated>
//------------------------------------------------------------------------------

using Gioco1;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace Gioco1 {
    
    
    /// <summary>
    /// InizioGioco
    /// </summary>
    public partial class InizioGioco : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 10 "..\..\InizioGioco.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label l1;
        
        #line default
        #line hidden
        
        
        #line 11 "..\..\InizioGioco.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button w;
        
        #line default
        #line hidden
        
        
        #line 12 "..\..\InizioGioco.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button s;
        
        #line default
        #line hidden
        
        
        #line 13 "..\..\InizioGioco.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button a;
        
        #line default
        #line hidden
        
        
        #line 14 "..\..\InizioGioco.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button d;
        
        #line default
        #line hidden
        
        
        #line 15 "..\..\InizioGioco.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button Avanti;
        
        #line default
        #line hidden
        
        
        #line 16 "..\..\InizioGioco.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button Indietro;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/Gioco1;component/iniziogioco.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\InizioGioco.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.l1 = ((System.Windows.Controls.Label)(target));
            return;
            case 2:
            this.w = ((System.Windows.Controls.Button)(target));
            return;
            case 3:
            this.s = ((System.Windows.Controls.Button)(target));
            return;
            case 4:
            this.a = ((System.Windows.Controls.Button)(target));
            return;
            case 5:
            this.d = ((System.Windows.Controls.Button)(target));
            return;
            case 6:
            this.Avanti = ((System.Windows.Controls.Button)(target));
            
            #line 15 "..\..\InizioGioco.xaml"
            this.Avanti.Click += new System.Windows.RoutedEventHandler(this.Avanti_Click);
            
            #line default
            #line hidden
            return;
            case 7:
            this.Indietro = ((System.Windows.Controls.Button)(target));
            
            #line 16 "..\..\InizioGioco.xaml"
            this.Indietro.Click += new System.Windows.RoutedEventHandler(this.Avanti_Click);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

