﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Gioco1
{
    /// <summary>
    /// Logica di interazione per FineLivello1Finestra.xaml
    /// </summary>
    public partial class FineLivello1Finestra : Window
    {
        public double tempo;
        public FineLivello1Finestra()
        {
            WindowState = WindowState.Maximized;
            InitializeComponent();
        }

        private void On_Click(object sender, RoutedEventArgs e)
        {
            Livello1 l1 = new Livello1();
            Livello2 l2 = new Livello2();
            Button b = new Button();
            b = (Button)sender;
            if (b == Ricomincia)
            {
                tempo = 0;
                this.Hide();
                l1.ShowDialog();
            }
            if (b == Avanti)
            {
                l2.tempo = tempo;
                this.Hide();
                l2.ShowDialog();
            }
        }
    }
}
