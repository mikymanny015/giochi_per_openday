﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace Gioco1
{
    /// <summary>
    /// Logica di interazione per Livello3.xaml
    /// </summary>
    public partial class Livello3 : Window
    {
        // elementi pricipali
        Button Pedone = new Button();
        Button Arrivo = new Button();
        // ostacoli fissi
        Button b1 = new Button();
        Button b2 = new Button();
        Button b3 = new Button();
        Button b4 = new Button();
        Button b5 = new Button();
        Button b6 = new Button();
        Button b7 = new Button();
        Button b8 = new Button();
        Button b9 = new Button();
        Button b10 = new Button();
        Button b11 = new Button();
        Button b12 = new Button();
        Button b13 = new Button();
        Button b14 = new Button();
        Button b15 = new Button();
        Button b16 = new Button();
        Button b17 = new Button();
        Button b18 = new Button();
        Button b19 = new Button();
        Button b20 = new Button();
        Button b21 = new Button();
        Button b22 = new Button();
        Button b23 = new Button();
        Button b24 = new Button();
        Button b25 = new Button();
        Button b26 = new Button();
        Button b27 = new Button();
        Button b28 = new Button();
        Button b29 = new Button();
        Button b30 = new Button();
        Button b31 = new Button();
        Button b32 = new Button();
        Button b33 = new Button();
        Button b34 = new Button();
        Button b35 = new Button();
        Button b36 = new Button();
        Button b37 = new Button();
        Button b38 = new Button();
        Button b39 = new Button();
        Button b40 = new Button();
        Button b41 = new Button();
        Button b42 = new Button();
        Button b43 = new Button();
        // ostacoli mobili 
        Button bm1 = new Button();
        Button bm2 = new Button();
        Button bm3 = new Button();
        Button bm4 = new Button();
        Button bm5 = new Button();
        Button bm6 = new Button();
        Button bm7 = new Button();
        Button bm8 = new Button();
        Button bm9 = new Button();
        DispatcherTimer timer = new DispatcherTimer();
        public double tempo;
        public Livello3()
        {
            Grid miogrid = new Grid();
            miogrid.Background = Brushes.Black;
            for(int i=0; i<10; i++)
            {
                miogrid.ColumnDefinitions.Add(new ColumnDefinition());
            }
            for(int i = 0; i < 10; i++)
            {
                miogrid.RowDefinitions.Add(new RowDefinition());
            }
            miogrid.ShowGridLines = true;
            timer.Interval = TimeSpan.FromMilliseconds(0);
            timer.Tick += Timer_Tick;
            // aggiungo i figli
            miogrid.Children.Add(Pedone);
            miogrid.Children.Add(Arrivo);
            // ostacoli 
            miogrid.Children.Add(b1);
            miogrid.Children.Add(b2);
            miogrid.Children.Add(b3);
            miogrid.Children.Add(b4);
            miogrid.Children.Add(b5);
            miogrid.Children.Add(b6);
            miogrid.Children.Add(b7);
            miogrid.Children.Add(b8);
            miogrid.Children.Add(b9);
            miogrid.Children.Add(b10);
            miogrid.Children.Add(b11);
            miogrid.Children.Add(b12);
            miogrid.Children.Add(b13);
            miogrid.Children.Add(b14);
            miogrid.Children.Add(b15);
            miogrid.Children.Add(b16);
            miogrid.Children.Add(b17);
            miogrid.Children.Add(b18);
            miogrid.Children.Add(b19);
            miogrid.Children.Add(b20);
            miogrid.Children.Add(b21);
            miogrid.Children.Add(b22);
            miogrid.Children.Add(b23);
            miogrid.Children.Add(b24);
            miogrid.Children.Add(b25);
            miogrid.Children.Add(b26);
            miogrid.Children.Add(b27);
            miogrid.Children.Add(b28);
            miogrid.Children.Add(b29);
            miogrid.Children.Add(b30);
            miogrid.Children.Add(b31);
            miogrid.Children.Add(b32);
            miogrid.Children.Add(b33);
            miogrid.Children.Add(b34);
            miogrid.Children.Add(b35);
            miogrid.Children.Add(b36);
            miogrid.Children.Add(b37);
            miogrid.Children.Add(b38);
            miogrid.Children.Add(b39);
            miogrid.Children.Add(b40);
            miogrid.Children.Add(b41);
            miogrid.Children.Add(b42);
            miogrid.Children.Add(b43);
            // ostacoli mobili 
            miogrid.Children.Add(bm1);
            miogrid.Children.Add(bm2);
            miogrid.Children.Add(bm3);
            miogrid.Children.Add(bm4);
            miogrid.Children.Add(bm5);
            miogrid.Children.Add(bm6);
            miogrid.Children.Add(bm7);
            miogrid.Children.Add(bm8);
            miogrid.Children.Add(bm9);
            // inizializzo i componenti 
            Inizializza();
            WindowState = WindowState.Maximized;
            InitializeComponent();
            grid.AddChild(miogrid);
            grid.KeyDown += Direzione;
            timer.Start();
        }
        private void Timer_Tick(object sender, EventArgs e)
        {
            double t;
            timer.Interval += TimeSpan.FromMilliseconds(100);
            b3.Content = timer.Interval;
            t = timer.Interval.TotalMilliseconds;
            t = t / 100;
            if (t % 2 != 0)
            {
                int br = Grid.GetRow(Pedone);
                int bc = Grid.GetColumn(Pedone);
                //bm1
                if ((bc == 5) && (br == 0))
                {
                    Grid.SetColumn(Pedone, 3);
                    Grid.SetRow(Pedone, 0);
                }
                //bm2
                if ((bc == 3) && (br == 3))
                {
                    Grid.SetColumn(Pedone, 5);
                    Grid.SetRow(Pedone, 3);
                }
                //bm3
                if ((bc == 5) && (br == 5))
                {
                    Grid.SetColumn(Pedone, 3);
                    Grid.SetRow(Pedone, 5);
                }
                //bm4
                if ((bc == 3) && (br == 7))
                {
                    Grid.SetColumn(Pedone, 5);
                    Grid.SetRow(Pedone, 7);
                }
                //bm5
                if ((bc == 5) && (br == 9))
                {
                    Grid.SetColumn(Pedone, 3);
                    Grid.SetRow(Pedone, 9);
                }
                //bm6
                if ((bc == 6) && (br == 3))
                {
                    Grid.SetColumn(Pedone, 8);
                    Grid.SetRow(Pedone, 3);
                }
                //bm7
                if ((bc == 8) && (br ==5))
                {
                    Grid.SetColumn(Pedone, 6);
                    Grid.SetRow(Pedone, 5);
                }
                // bm8
                if ((bc == 6) && (br == 7))
                {
                    Grid.SetColumn(Pedone, 8);
                    Grid.SetRow(Pedone, 7);
                }
                //bm9
                if ((bc == 8) && (br == 9))
                {
                    Grid.SetColumn(Pedone, 6);
                    Grid.SetRow(Pedone, 9);
                }
            }
        }
        private void Direzione(object sender,KeyEventArgs e)
        {
            int bc = Grid.GetColumn(Pedone);
            int br = Grid.GetRow(Pedone);
            if (e.Key.Equals(Key.W))// setto la w che va in alto 
            {
                if (br == 0)
                {
                    Grid.SetRow(Pedone, 0);
                }
                else
                    Grid.SetRow(Pedone, br- 1);
                br = 0;
            }
            if (e.Key.Equals(Key.S))// s va in basso 
            {
                if (br == 9)
                {
                    Grid.SetRow(Pedone, 9);
                }
                else
                    Grid.SetRow(Pedone, br + 1);
                br = 0;
            }
            if (e.Key.Equals(Key.A))// a va a destra 
            {
                if (bc == 0)
                {
                    Grid.SetColumn(Pedone, 0);
                }
                else
                    Grid.SetColumn(Pedone, bc - 1);
                bc = 0;
            }
            if (e.Key.Equals(Key.D))
            {
                if (bc == 9)
                {
                    Grid.SetColumn(Pedone, 9);
                }
                else
                    Grid.SetColumn(Pedone, bc +1);
                bc = 0;
            }
            Gestione_ostacoli();
        }
        private void Gestione_ostacoli()
        {
            int br = Grid.GetRow(Pedone);
            int bc = Grid.GetColumn(Pedone);
            // arrivo 
            if ((bc == 0) && (br == 9))
            {
                Finestra_finale ff = new Finestra_finale();
                tempo += timer.Interval.TotalMilliseconds;
                ff.titolo.Content = "finito il gioco, il tempo che hai impiegato è\n"+ timer.Interval+" millisecondi"+"\n il tempo totale è "+tempo+" millisecondi";
                this.Hide();
                ff.ShowDialog();
            }
            // ostacoli 
            //b1
            if ((bc == 8) && (br == 1))
            {
                Grid.SetColumn(Pedone, 9);
                Grid.SetRow(Pedone, 1);
            }
            //b2
            if ((bc == 6) && (br == 0))
            {
                Grid.SetColumn(Pedone, 7);
                Grid.SetRow(Pedone, 0);
            }
            //b3
            if ((bc == 2) && (br == 0))
            {
                Grid.SetColumn(Pedone, 3);
                Grid.SetRow(Pedone, 0);
            }
            //b4
            if ((bc == 1) && (br == 0))
            {
                Grid.SetColumn(Pedone, 1);
                Grid.SetRow(Pedone, 1);
            }
            //b5
            if ((bc == 0) && (br == 1))
            {
                Grid.SetColumn(Pedone, 1);
                Grid.SetRow(Pedone, 1);
            }
            //b6
            if ((bc == 0) && (br == 2))
            {
                Grid.SetColumn(Pedone, 1);
                Grid.SetRow(Pedone, 2);
            }
            //b7
            if ((bc == 0) && (br == 3))
            {
                Grid.SetColumn(Pedone, 1);
                Grid.SetRow(Pedone, 3);
            }
            //b8
            if ((bc == 2) && (br == 2))
            {
                Grid.SetColumn(Pedone, 2);
                Grid.SetRow(Pedone, 1);
            }
            //b9
            if ((bc == 3) && (br == 2))
            {
                Grid.SetColumn(Pedone, 3);
                Grid.SetRow(Pedone, 1);
            }
            // b10
            if ((bc == 4) && (br == 2))
            {
                Grid.SetColumn(Pedone, 4);
                Grid.SetRow(Pedone, 3);
            }
            // b11
            if ((bc == 4) && (br == 1))
            {
                Grid.SetColumn(Pedone, 5);
                Grid.SetRow(Pedone, 1);
            }
            //b12
            if ((bc == 5) && (br == 2))
            {
                Grid.SetColumn(Pedone, 5);
                Grid.SetRow(Pedone, 1);
            }
            //b13
            if ((bc == 6) && (br == 2))
            {
                Grid.SetColumn(Pedone, 6);
                Grid.SetRow(Pedone, 1);
            }
            //b14
            if ((bc == 7) && (br == 2))
            {
                Grid.SetColumn(Pedone, 7);
                Grid.SetRow(Pedone, 1);
            }
            // b15
            if ((bc == 9) && (br == 3))
            {
                Grid.SetColumn(Pedone, 9);
                Grid.SetRow(Pedone, 2);
            }
            //b16
            if ((bc == 9) && (br == 4))
            {
                Grid.SetColumn(Pedone, 8);
                Grid.SetRow(Pedone, 4);
            }
            //b17
            if ((bc == 9) && (br == 5))
            {
                Grid.SetColumn(Pedone, 8);
                Grid.SetRow(Pedone, 5);
            }
            //b18
            if ((bc == 7) && (br == 4))
            {
                Grid.SetColumn(Pedone, 8);
                Grid.SetRow(Pedone, 4);
            }
            //b19
            if ((bc == 6) && (br == 4))
            {
                Grid.SetColumn(Pedone, 6);
                Grid.SetRow(Pedone, 3);
            }
            //20
            if ((bc == 5) && (br == 4))
            {
                Grid.SetColumn(Pedone, 5);
                Grid.SetRow(Pedone, 3);
            }
            //21
            if ((bc == 4) && (br == 4))
            {
                Grid.SetColumn(Pedone, 4);
                Grid.SetRow(Pedone, 3);
            }
            //22
            if ((bc == 3) && (br == 4))
            {
                Grid.SetColumn(Pedone, 3);
                Grid.SetRow(Pedone, 3);
            }
            //23
            if ((bc == 2) && (br == 4))
            {
                Grid.SetColumn(Pedone, 2);
                Grid.SetRow(Pedone, 3);
            }
            //b24
            if ((bc == 1) && (br == 4))
            {
                Grid.SetColumn(Pedone, 1);
                Grid.SetRow(Pedone, 3);
            }
            //b25
            if ((bc == 0) && (br == 5))
            {
                Grid.SetColumn(Pedone, 1);
                Grid.SetRow(Pedone, 5);
            }
            //b26
            if ((bc == 0) && (br == 6))
            {
                Grid.SetColumn(Pedone, 1);
                Grid.SetRow(Pedone, 6);
            }
            //b27
            if ((bc == 0) && (br == 7))
            {
                Grid.SetColumn(Pedone, 1);
                Grid.SetRow(Pedone, 7);
            }
            //b28
            if ((bc == 2) && (br == 6))
            {
                Grid.SetColumn(Pedone, 2);
                Grid.SetRow(Pedone, 5);
            }
            //b29
            if ((bc == 3) && (br == 6))
            {
                Grid.SetColumn(Pedone, 3);
                Grid.SetRow(Pedone, 5);
            }
            //b30
            if ((bc == 4) && (br == 6))
            {
                Grid.SetColumn(Pedone, 4);
                Grid.SetRow(Pedone, 5);
            }
            //b31
            if ((bc == 5) && (br == 6))
            {
                Grid.SetColumn(Pedone, 5);
                Grid.SetRow(Pedone, 5);
            }
            //b32
            if ((bc == 6) && (br == 6))
            {
                Grid.SetColumn(Pedone, 6);
                Grid.SetRow(Pedone, 5);
            }
            //b33
            if ((bc == 7) && (br == 6))
            {
                Grid.SetColumn(Pedone, 7);
                Grid.SetRow(Pedone, 7);
            }
            //b34
            if ((bc == 8) && (br == 6))
            {
                Grid.SetColumn(Pedone, 8);
                Grid.SetRow(Pedone, 5);
            }
            //b35
            if ((bc == 8) && (br == 8))
            {
                Grid.SetColumn(Pedone, 8);
                Grid.SetRow(Pedone, 7);
            }
            //b36
            if ((bc == 7) && (br == 8))
            {
                Grid.SetColumn(Pedone, 7);
                Grid.SetRow(Pedone, 7);
            }
            //b37
            if ((bc == 6) && (br == 8))
            {
                Grid.SetColumn(Pedone, 6);
                Grid.SetRow(Pedone, 7);
            }
            //b38
            if ((bc == 5) && (br == 8))
            {
                Grid.SetColumn(Pedone, 5);
                Grid.SetRow(Pedone, 7);
            }
            //b39
            if ((bc == 4) && (br == 8))
            {
                Grid.SetColumn(Pedone, 4);
                Grid.SetRow(Pedone, 7);
            }
            // b40
            if ((bc == 3) && (br == 8))
            {
                Grid.SetColumn(Pedone, 3);
                Grid.SetRow(Pedone, 7);
            }
            //b41
            if ((bc == 2) && (br == 8))
            {
                Grid.SetColumn(Pedone, 2);
                Grid.SetRow(Pedone, 7);
            }
            //b42
            if ((bc == 1) && (br == 8))
            {
                Grid.SetColumn(Pedone, 1);
                Grid.SetRow(Pedone, 7);
            }
            //b43 
            if ((bc == 8) && (br == 2))
            {
                Grid.SetColumn(Pedone, 9);
                Grid.SetRow(Pedone, 2);
            }

            // ostacoli mobili 
            if ((bc == 4) && (br == 0))
            {
                Grid.SetColumn(Pedone, 5);
                Grid.SetRow(Pedone, 0);
            }
            //bm2
            if ((bc == 4) && (br == 3))
            {
                Grid.SetColumn(Pedone, 3);
                Grid.SetRow(Pedone, 3);
            }
            //bm3
            if ((bc == 4) && (br == 5))
            {
                Grid.SetColumn(Pedone, 5);
                Grid.SetRow(Pedone, 5);
            }
            //bm4
            if ((bc == 4) && (br == 7))
            {
                Grid.SetColumn(Pedone, 3);
                Grid.SetRow(Pedone, 7);
            }
            //bm5
            if ((bc == 4) && (br == 9))
            {
                Grid.SetColumn(Pedone, 5);
                Grid.SetRow(Pedone, 9);
            }
            //bm6
            if ((bc == 7) && (br == 3))
            {
                Grid.SetColumn(Pedone, 6);
                Grid.SetRow(Pedone, 3);
            }
            //bm7
            if ((bc == 7) && (br == 5))
            {
                Grid.SetColumn(Pedone, 8);
                Grid.SetRow(Pedone, 5);
            }
            // bm8
            if ((bc == 7) && (br == 7))
            {
                Grid.SetColumn(Pedone, 6);
                Grid.SetRow(Pedone, 7);
            }
            //bm9
            if ((bc == 7) && (br == 9))
            {
                Grid.SetColumn(Pedone, 8);
                Grid.SetRow(Pedone, 9);
            }
        }
        private void Inizializza()
        {
            // setto pedone
            Grid.SetColumn(Pedone, 9);
            Grid.SetRow(Pedone, 0);
            Pedone.Background = Brushes.Green;
            Pedone.Content = "Player";
            // setto arrivo 
            Grid.SetColumn(Arrivo, 0);
            Grid.SetRow(Arrivo, 9);
            Arrivo.Background = Brushes.Blue;
            Arrivo.Content = "Arrivo";
            //setto ostacoli 
            // b1
            Grid.SetColumn(b1, 8);
            Grid.SetRow(b1, 1);
            b1.Background = Brushes.Red;
            // b2
            Grid.SetColumn(b2, 6);
            Grid.SetRow(b2, 0);
            b2.Background = Brushes.Red;
            // b3 
            Grid.SetColumn(b3, 2);
            Grid.SetRow(b3, 0);
            b3.Background = Brushes.Red;
            // b4
            Grid.SetColumn(b4, 1);
            Grid.SetRow(b4, 0);
            b4.Background = Brushes.Red;
            // b5
            Grid.SetColumn(b5, 0);
            Grid.SetRow(b5, 1);
            b5.Background = Brushes.Red;
            // b6
            Grid.SetColumn(b6, 0);
            Grid.SetRow(b6, 2);
            b6.Background = Brushes.Red;
            // b7
            Grid.SetColumn(b7, 0);
            Grid.SetRow(b7, 3);
            b7.Background = Brushes.Red;
            // b8
            Grid.SetColumn(b8, 2);
            Grid.SetRow(b8, 2);
            b8.Background = Brushes.Red;
            //b9
            Grid.SetColumn(b9, 3);
            Grid.SetRow(b9, 2);
            b9.Background = Brushes.Red;
            // 10
            Grid.SetColumn(b10, 4);
            Grid.SetRow(b10, 2);
            b10.Background = Brushes.Red;
            // 11
            Grid.SetColumn(b11, 4);
            Grid.SetRow(b11, 1);
            b11.Background = Brushes.Red;
            // 12
            Grid.SetColumn(b12, 5);
            Grid.SetRow(b12, 2);
            b12.Background = Brushes.Red;
            // 13
            Grid.SetColumn(b13, 6);
            Grid.SetRow(b13, 2);
            b13.Background = Brushes.Red;
            // 14 
            Grid.SetColumn(b14, 7);
            Grid.SetRow(b14, 2);
            b14.Background = Brushes.Red;
            // 15 
            Grid.SetColumn(b15, 9);
            Grid.SetRow(b15, 3);
            b15.Background = Brushes.Red;
            // 16
            Grid.SetColumn(b16, 9);
            Grid.SetRow(b16, 4);
            b16.Background = Brushes.Red;
            // 17
            Grid.SetColumn(b17, 9);
            Grid.SetRow(b17, 5);
            b17.Background = Brushes.Red;
            // 18
            Grid.SetColumn(b18, 7);
            Grid.SetRow(b18, 4);
            b18.Background = Brushes.Red;
            //19
            Grid.SetColumn(b19, 6);
            Grid.SetRow(b19, 4);
            b19.Background = Brushes.Red;
            // 20
            Grid.SetColumn(b20, 5);
            Grid.SetRow(b20, 4);
            b20.Background = Brushes.Red;
            //21
            Grid.SetColumn(b21, 4);
            Grid.SetRow(b21, 4);
            b21.Background = Brushes.Red;
            // 22 
            Grid.SetColumn(b22, 3);
            Grid.SetRow(b22, 4);
            b22.Background = Brushes.Red;
            // 23
            Grid.SetColumn(b23, 2);
            Grid.SetRow(b23, 4);
            b23.Background = Brushes.Red;
            //24
            Grid.SetColumn(b24, 1);
            Grid.SetRow(b24, 4);
            b24.Background = Brushes.Red;
            //25
            Grid.SetColumn(b25, 0);
            Grid.SetRow(b25, 5);
            b25.Background = Brushes.Red;
            //26
            Grid.SetColumn(b26, 0);
            Grid.SetRow(b26, 6);
            b26.Background = Brushes.Red;
            //27
            Grid.SetColumn(b27, 0);
            Grid.SetRow(b27, 7);
            b27.Background = Brushes.Red;
            //28
            Grid.SetColumn(b28, 2);
            Grid.SetRow(b28, 6);
            b28.Background = Brushes.Red;
            //29
            Grid.SetColumn(b29, 3);
            Grid.SetRow(b29, 6);
            b29.Background = Brushes.Red;
            // 30 
            Grid.SetColumn(b30, 4);
            Grid.SetRow(b30, 6);
            b30.Background = Brushes.Red;
            //31
            Grid.SetColumn(b31, 5);
            Grid.SetRow(b31, 6);
            b31.Background = Brushes.Red;
            // 32
            Grid.SetColumn(b32, 6);
            Grid.SetRow(b32, 6);
            b32.Background = Brushes.Red;
            //33
            Grid.SetColumn(b33, 7);
            Grid.SetRow(b33, 6);
            b33.Background = Brushes.Red;
            //34
            Grid.SetColumn(b34, 8);
            Grid.SetRow(b34, 6);
            b34.Background = Brushes.Red;
            // 35 
            Grid.SetColumn(b35, 8);
            Grid.SetRow(b35, 8);
            b35.Background = Brushes.Red;
            // 36
            Grid.SetColumn(b36, 7);
            Grid.SetRow(b36, 8);
            b36.Background = Brushes.Red;
            // 37 
            Grid.SetColumn(b37, 6);
            Grid.SetRow(b37, 8);
            b37.Background = Brushes.Red;
            //38
            Grid.SetColumn(b38, 5);
            Grid.SetRow(b38, 8);
            b38.Background = Brushes.Red;
            //39
            Grid.SetColumn(b39, 4);
            Grid.SetRow(b39, 8);
            b39.Background = Brushes.Red;
            // 40 
            Grid.SetColumn(b40, 3);
            Grid.SetRow(b40, 8);
            b40.Background = Brushes.Red;
            //41
            Grid.SetColumn(b41, 2);
            Grid.SetRow(b41, 8);
            b41.Background = Brushes.Red;
            //42
            Grid.SetColumn(b42, 1);
            Grid.SetRow(b42, 8);
            b42.Background = Brushes.Red;
            //43
            Grid.SetColumn(b43, 8);
            Grid.SetRow(b43, 2);
            b43.Background = Brushes.Red;
            // ostacoli mobili 
            //bm1
            Grid.SetColumn(bm1, 4);
            Grid.SetRow(bm1, 0);
            bm1.Background = Brushes.Yellow;
            //bm2
            Grid.SetColumn(bm2, 4);
            Grid.SetRow(bm2, 3);
            bm2.Background = Brushes.Yellow;
            //bm3
            Grid.SetColumn(bm3, 4);
            Grid.SetRow(bm3, 5);
            bm3.Background = Brushes.Yellow;
            //bm4
            Grid.SetColumn(bm4, 4);
            Grid.SetRow(bm4, 7);
            bm4.Background = Brushes.Yellow;
            // bm5
            Grid.SetColumn(bm5, 4);
            Grid.SetRow(bm5, 9);
            bm5.Background = Brushes.Yellow;
            // bm6 
            Grid.SetColumn(bm6, 7);
            Grid.SetRow(bm6, 3);
            bm6.Background = Brushes.Yellow;
            // bm7 
            Grid.SetColumn(bm7, 7);
            Grid.SetRow(bm7, 5);
            bm7.Background = Brushes.Yellow;
            // bm8 
            Grid.SetColumn(bm8, 7);
            Grid.SetRow(bm8, 7);
            bm8.Background = Brushes.Yellow;
            //bm9 
            Grid.SetColumn(bm9, 7);
            Grid.SetRow(bm9, 9);
            bm9.Background = Brushes.Yellow;
        }
    }
}
