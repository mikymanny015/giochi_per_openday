﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace Gioco1
{
    /// <summary>
    /// Logica di interazione per Livello0.xaml
    /// </summary>
    public partial class Livello0 : Window
    {
        Button B1 = new Button();// pedina
                                 //ostacoli 
        Button B2 = new Button();
        Button B3 = new Button();
        Button B4 = new Button();
        Button B5 = new Button();
        Button B6 = new Button();
        Button B7 = new Button();
        Button B8 = new Button();
        Button B9 = new Button();
        Button B10 = new Button();
        Button B11 = new Button();
        Button B12 = new Button();
        Button B13 = new Button();
        Button B14 = new Button();
        Button B15 = new Button();
        Button B16 = new Button();
        Button B17 = new Button();
        Button B18 = new Button();
        // bottone arrivo 
        Button B19 = new Button();
        // bottoni per rendere più difficile 
        Button B20 = new Button();
        Button B21= new Button();
        Button B22 = new Button();
        Button B23 = new Button();
        Button B24 = new Button();
        Button B25 = new Button();

        Button cont = new Button();
        //Label Label = new Label();
        DispatcherTimer t1 = new DispatcherTimer();
        public Livello0()
        {
            t1.Interval = TimeSpan.FromMilliseconds(0);
            t1.Tick += T1_Tick;
            Grid miogrid = new Grid();
            miogrid.Background = Brushes.Green;
            for (int i = 0; i < 10; i++)
            {
                miogrid.ColumnDefinitions.Add(new ColumnDefinition());
                miogrid.Background = Brushes.Black;
            }
            for (int i = 0; i < 10; i++)
            {
                miogrid.RowDefinitions.Add(new RowDefinition());
            }
            miogrid.ShowGridLines = true;
            // giocatore 
            miogrid.Children.Add(B1);
            // ostacoli 
            miogrid.Children.Add(B2);
            miogrid.Children.Add(B3);
            miogrid.Children.Add(B4);
            miogrid.Children.Add(B5);
            miogrid.Children.Add(B6);
            miogrid.Children.Add(B7);
            miogrid.Children.Add(B8);
            miogrid.Children.Add(B9);
            miogrid.Children.Add(B10);
            miogrid.Children.Add(B11);
            miogrid.Children.Add(B12);
            miogrid.Children.Add(B13);
            miogrid.Children.Add(B14);
            miogrid.Children.Add(B15);
            miogrid.Children.Add(B16);
            miogrid.Children.Add(B17);
            miogrid.Children.Add(B18);
            miogrid.Children.Add(B19);
            miogrid.Children.Add(B20);
            miogrid.Children.Add(B21);
            miogrid.Children.Add(B22);
            miogrid.Children.Add(B23);
            miogrid.Children.Add(B24);
            miogrid.Children.Add(B25);
            miogrid.Children.Add(cont);
            // lable 
            //  miogrid.Children.Add(Label);
            Inizializaa();
            WindowState = WindowState.Maximized;
            InitializeComponent();
            mg.AddChild(miogrid);
            t1.Start();
            mg.KeyDown += Direzione;
        }
        private void T1_Tick(object sender, EventArgs e)
        {
            t1.Interval += TimeSpan.FromMilliseconds(100);
            cont.Content = t1.Interval;
        }
        void Direzione(object sender, KeyEventArgs e)
        {
            //  B1.FontSize = 7;
            //  B1.Content = "hello";
            //B1.FontSize = 15;
            // ostacolo c=6, r=7
            int bc;
            int br;
            if (e.Key.Equals(Key.W))
            {
                br = Grid.GetRow(B1);
                if (br <= 0)
                {
                    br = 0;
                    Grid.SetRow(B1, br);
                }
                else
                    Grid.SetRow(B1, br - 1);
                br = 0;
            }
            if (e.Key.Equals(Key.S))
            {
                br = Grid.GetRow(B1);
                if (br >= 9)
                {
                    br = 9;
                    Grid.SetRow(B1, br);
                }
                else
                    Grid.SetRow(B1, br + 1);
                br = 0;
            }
            if (e.Key.Equals(Key.D))
            {
                bc = Grid.GetColumn(B1);
                if (bc >= 9)
                {
                    bc = 9;
                    Grid.SetColumn(B1, bc);
                }
                else
                    Grid.SetColumn(B1, bc + 1);
                bc = 0;
            }
            if (e.Key.Equals(Key.A))
            {
                bc = Grid.GetColumn(B1);
                if (bc <= 0)
                {
                    bc = 0;
                    Grid.SetColumn(B1, bc);
                }
                else
                    Grid.SetColumn(B1, bc - 1);
                bc = 0;
            }
            Gestioneostacoli(B1);
        }
        private void Inizializaa()
        {
            Grid.SetColumn(B1, 0);
            Grid.SetRow(B1, 0);
            B1.Background = Brushes.Green;
            B1.Content = "player";
            B1.FontSize = 15;
            //contatore 
            Grid.SetColumn(cont, 3);
            Grid.SetRow(cont, 0);
            cont.Background = Brushes.Red;
            // ostacoli 
            Grid.SetColumn(B2, 6);
            Grid.SetRow(B2, 7);
            B2.Background = Brushes.Red;
            //b3
            Grid.SetColumn(B3, 5);
            Grid.SetRow(B3, 6);
            B3.Background = Brushes.Red;
            //b4
            Grid.SetColumn(B4, 4);
            Grid.SetRow(B4, 5);
            B4.Background = Brushes.Red;
            // b5 
            Grid.SetColumn(B5, 3);
            Grid.SetRow(B5, 4);
            B5.Background = Brushes.Red;
            //b6
            Grid.SetColumn(B6, 2);
            Grid.SetRow(B6, 3);
            B6.Background = Brushes.Red;
            //b7 
            Grid.SetColumn(B7, 1);
            Grid.SetRow(B7, 2);
            B7.Background = Brushes.Red;
            // b8 
            Grid.SetColumn(B8, 1);
            Grid.SetRow(B8, 1);
            B8.Background = Brushes.Red;
            //b9 
            Grid.SetColumn(B9, 1);
            Grid.SetRow(B9, 0);
            B9.Background = Brushes.Red;
            // b10 
            Grid.SetColumn(B10, 9);
            Grid.SetRow(B10, 9);
            B10.Background = Brushes.Red;
            // b11
            Grid.SetColumn(B11, 8);
            Grid.SetRow(B11, 8);
            B11.Background = Brushes.Red;
            // b12
            Grid.SetColumn(B12, 8);
            Grid.SetRow(B12, 7);
            B12.Background = Brushes.Red;
            // b13
            Grid.SetColumn(B13, 8);
            Grid.SetRow(B13, 6);
            B13.Background = Brushes.Red;
            // b14
            Grid.SetColumn(B14, 8);
            Grid.SetRow(B14, 5);
            B14.Background = Brushes.Red;
            //b15
            Grid.SetColumn(B15, 8);
            Grid.SetRow(B15, 3);
            B15.Background = Brushes.Red;
            //b16
            Grid.SetColumn(B16, 8);
            Grid.SetRow(B16, 2);
            B16.Background = Brushes.Red;
            //b17
            Grid.SetColumn(B17, 8);
            Grid.SetRow(B17, 1);
            B17.Background = Brushes.Red;
            //b18
            Grid.SetColumn(B18, 8);
            Grid.SetRow(B18, 0);
            B18.Background = Brushes.Red;
            // B19
            Grid.SetColumn(B19, 9);
            Grid.SetRow(B19, 0);
            B19.FontSize = 12;
            B19.Content = "Arrivo";
            B19.Background = Brushes.Blue;
            // B20
            Grid.SetColumn(B20, 0);
            Grid.SetRow(B20, 4);
            B20.Background = Brushes.Red;
            //B21
            Grid.SetColumn(B21, 1);
            Grid.SetRow(B21, 5);
            B21.Background = Brushes.Red;
            //B22
            Grid.SetColumn(B22, 2);
            Grid.SetRow(B22, 6);
            B22.Background = Brushes.Red;
            //B23
            Grid.SetColumn(B23, 3);
            Grid.SetRow(B23, 7);
            B23.Background = Brushes.Red;
            //B24
            Grid.SetColumn(B24, 4);
            Grid.SetRow(B24, 8);
            B24.Background = Brushes.Red;
            //B25
            Grid.SetColumn(B25, 5);
            Grid.SetRow(B25, 10);
            B25.Background = Brushes.Red;
        }
        private void Gestioneostacoli(Button b1)
        {
            // gestione cont c  3 r 0
            if ((Grid.GetColumn(B1) == 3) && (Grid.GetRow(B1) == 0))
            {
                Grid.SetColumn(B1, 2);
                Grid.SetRow(B1, 0);
            }
            // gestione ostacoli 
            if ((Grid.GetColumn(B1) == 6) && (Grid.GetRow(B1) == 7))
            {
                Grid.SetColumn(B1, 5);
                Grid.SetRow(B1, 7);
            }
            if ((Grid.GetColumn(B1) == 5) && (Grid.GetRow(B1) == 6))
            {
                Grid.SetColumn(B1, 4);
                Grid.SetRow(B1, 6);
            }
            if ((Grid.GetColumn(B1) == 4) && (Grid.GetRow(B1) == 5))
            {
                Grid.SetColumn(B1, 3);
                Grid.SetRow(B1, 5);
            }
            if ((Grid.GetColumn(B1) == 3) && (Grid.GetRow(B1) == 4))
            {
                Grid.SetColumn(B1, 2);
                Grid.SetRow(B1, 4);
            }
            if ((Grid.GetColumn(B1) == 2) && (Grid.GetRow(B1) == 3))
            {
                Grid.SetColumn(B1, 1);
                Grid.SetRow(B1, 3);
            }
            if ((Grid.GetColumn(B1) == 1) && (Grid.GetRow(B1) == 2))
            {
                Grid.SetColumn(B1, 0);
                Grid.SetRow(B1, 2);
            }
            if ((Grid.GetColumn(B1) == 1) && (Grid.GetRow(B1) == 1))
            {
                Grid.SetColumn(B1, 0);
                Grid.SetRow(B1, 1);
            }
            if ((Grid.GetColumn(B1) == 1) && (Grid.GetRow(B1) == 0))
            {
                Grid.SetColumn(B1, 0);
                Grid.SetRow(B1, 0);
            }
            if ((Grid.GetColumn(B1) == 9) && (Grid.GetRow(B1) == 9))
            {
                Grid.SetColumn(B1, 8);
                Grid.SetRow(B1, 9);
            }
            if ((Grid.GetColumn(B1) == 8) && (Grid.GetRow(B1) == 8))
            {
                Grid.SetColumn(B1, 7);
                Grid.SetRow(B1, 8);
            }
            if ((Grid.GetColumn(B1) == 8) && (Grid.GetRow(B1) == 7))
            {
                Grid.SetColumn(B1, 7);
                Grid.SetRow(B1, 7);
            }
            if ((Grid.GetColumn(B1) == 8) && (Grid.GetRow(B1) == 6))
            {
                Grid.SetColumn(B1, 7);
                Grid.SetRow(B1, 6);
            }
            if ((Grid.GetColumn(B1) == 8) && (Grid.GetRow(B1) == 5))
            {
                Grid.SetColumn(B1, 7);
                Grid.SetRow(B1, 5);
            }
            if ((Grid.GetColumn(B1) == 8) && (Grid.GetRow(B1) == 3))
            {
                Grid.SetColumn(B1, 7);
                Grid.SetRow(B1, 3);
            }
            if ((Grid.GetColumn(B1) == 8) && (Grid.GetRow(B1) == 2))
            {
                Grid.SetColumn(B1, 7);
                Grid.SetRow(B1, 2);
            }
            if ((Grid.GetColumn(B1) == 8) && (Grid.GetRow(B1) == 1))
            {
                Grid.SetColumn(B1, 7);
                Grid.SetRow(B1, 1);
            }
            if ((Grid.GetColumn(B1) == 8) && (Grid.GetRow(B1) == 0))
            {
                Grid.SetColumn(B1, 7);
                Grid.SetRow(B1, 0);
            }
            // traguardo
            if ((Grid.GetColumn(B1) == 9) && (Grid.GetRow(B1) == 0))
            {
                B19.Content = "Arrivato";
                // finestra.Activate();
                B19.Background = Brushes.Yellow;
                // imposto il cambio di schermata
                Finestra finestra = new Finestra();
                finestra.Fine.Content = "finito il gioco, il tempo \n che hai  impiegato è \n" + t1.Interval + " millisecondi";
                finestra.tempo += t1.Interval.TotalMilliseconds;
                this.Hide();// chiudo la finestra  del gioco 
                finestra.ShowDialog(); // apro la finestra di fine 
            }
            if ((Grid.GetColumn(B1) == 0) && (Grid.GetRow(B1) == 4))
            {
                Grid.SetColumn(B1, 0);
                Grid.SetRow(B1, 3);
            }
            if ((Grid.GetColumn(B1) == 1) && (Grid.GetRow(B1) == 5))
            {
                Grid.SetColumn(B1, 1);
                Grid.SetRow(B1, 4);
            }
            if ((Grid.GetColumn(B1) == 2) && (Grid.GetRow(B1) == 6))
            {
                Grid.SetColumn(B1, 2);
                Grid.SetRow(B1, 5);
            }
            if ((Grid.GetColumn(B1) == 3) && (Grid.GetRow(B1) == 7))
            {
                Grid.SetColumn(B1, 3);
                Grid.SetRow(B1, 6);
            }
            if ((Grid.GetColumn(B1) == 4) && (Grid.GetRow(B1) == 8))
            {
                Grid.SetColumn(B1, 4);
                Grid.SetRow(B1, 7);
            }
            if ((Grid.GetColumn(B1) == 5) && (Grid.GetRow(B1) == 9))
            {
                Grid.SetColumn(B1, 5);// colonne 
                Grid.SetRow(B1, 8);// righe 
            }
        }
    }
}
